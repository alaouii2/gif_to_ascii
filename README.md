# Gif to ASCII art animation python (beta version)

Basic python script that turns a gif to a (sorta) ASCII art animation on terminal

### syntax

(Create a venv and install requirements first)
```console
$ python gif_to_ascii.py path/to/gif
```

### Example:

2 gifs are provided in the repository as example

```console
$ python gif_to_ascii.py samples/duane.gif
```
